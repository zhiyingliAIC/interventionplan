import sys
import os
import json
from collections import OrderedDict
import random
from datetime import datetime
from datetime import timedelta
from pathlib import Path
import bisect
import heapq
import collections
from myClass import lunar_calender
from myClass.system_para import myDates, paths
from myClass.extract_features import personalFeature, regionFeature
from myClass.intervention_plan import loadKnowledge
from myClass.intervention_plan import tips, exercises, acupoints, tea, foodTreatment, food, alert


#================== Functions ===================#


def save2json(path,filename,data_dict):
    #if append == 0:
     #   with open("%s/%s.json" % (path,filename), "w") as f:
     #       f.write(json.dumps(data_dict, indent=4, ensure_ascii=False))
    #else:
    full_filename = "%s/%s"% (path,filename)
    if os.path.isfile(full_filename):
        if os.stat(full_filename).st_size == 0:
            with open("%s/%s" % (path,filename), "w") as f:
                f.write(json.dumps({"data":[data_dict]}, indent=4, ensure_ascii=False))
        else:
            with open("%s/%s" % (path,filename), "r") as f:            
                data = json.load(f,object_pairs_hook=collections.OrderedDict)
                data["data"].append(data_dict)
            with open("%s/%s" % (path,filename), "w") as f:
                f.write(json.dumps(data, indent=4, ensure_ascii=False))
    else:
        with open("%s/%s" % (path,filename), "w") as f:
            array = []
            array.append(data_dict)
            json.dump({"data":array}, f, indent=4, ensure_ascii=False)
            
#=======================================================#
def getJson(path,filename):
    with open("%s/%s" % (path, filename) , "r") as f:
        json_data = json.load(f)
    return json_data
#=======================================================#



    

""" 



"""

__author__ = "Zhiying Li"
__copyright__ = "Wisdom AIC, 2019"

class make_directory(object):
    def __init__(self, user_info):
        self.userID = user_info["userID"]
        self.date_report = user_info["dateReportUTC"]
        self.date_class = myDates(self.date_report, 1)
        self.path_class = paths(self.date_class.starting_date(), self.userID)
        
    def output(self):
        path_output = self.path_class.path_output()
        return path_output
    
    def history(self):
        path_history = self.path_class.path_history()
        return path_history

class daily_report(object):    #format: yyyy-mm-dd #should be today's date
    def __init__(self, user_info):
        self.user_info = user_info
        self.userID = user_info["userID"]
        self.date_report = user_info["dateReportUTC"]
        self.return_fields = user_info["returnFields"]
        self.date_class = myDates(self.date_report, 1)  #we only generate intervention plan for one day
        self.path_class = paths(self.date_class.starting_date(), self.userID)
    
    def combine_knowledge(self, label_dict):
        '''This module combines intervention plans in different aspects, data_array is an array of intervention plans
        and labels is an array of their discriptions. The elements in labels serve as keys of the dictionaries in the combined array.'''
        
        final_dict = OrderedDict()
        value_tmp = label_dict[list(label_dict.keys())[0]]
        print (label_dict.keys())
        final_dict.update({"userID":value_tmp["userID"]})
        final_dict.update({"dateSolar":value_tmp["details"]["dateSolar"]})
        details = []
        for k, v in label_dict.items():
                
            dict_tmp = OrderedDict()
            del v["details"]["dateSolar"]
            dict_tmp.update({k:v["details"]})
            details.append(dict_tmp)
        final_dict.update({"details":details})
            
        return final_dict        

    def main(self):
        #make directory
        dir_class =  make_directory(self.user_info)
        history_path = dir_class.history()
        
        date_info = lunar_calender.getCalendar_range(self.date_class.starting_date(), self.date_class.end_date())[0]

        feature_person = personalFeature(self.user_info).main()
        
        save2json(history_path,"history_feature.json",feature_person)

        region = feature_person["region"]

        feature_region = regionFeature(date_info, region).main()

        tips_data = tips(feature_person, feature_region).main() #for single user
        exercise_data = exercises(feature_person, feature_region).main()
        acupoint_data = acupoints(feature_person, feature_region).main()
        tea_data = tea(feature_person, feature_region).main()
        food_treatment_data = foodTreatment(feature_person, feature_region).main()
        food_data = food(feature_person,feature_region).main()
        alert_data = alert(feature_person,feature_region).main()
        
        label_dict = {
            "tips":tips_data,
            "exercise":exercise_data,
            "acupoint":acupoint_data,
            "tea":tea_data,
            "foodTreatment":food_treatment_data,
            "food":food_data,
            "alert":alert_data
        }
        
    #    if len(self.return_fields) > 1 and "total" in self.return_fields:
    #        return("请确定好要返回的数据项！如果有total，就不要加其他的项。")
        if self.return_fields == []:
            self.return_fields.append("total")
        if "total" not in self.return_fields:
            del_array = label_dict.keys() - self.return_fields
            for d in del_array:
                del label_dict[d]
        
        combined_plan = self.combine_knowledge(label_dict)
        
        save2json(history_path,"history_intervention.json",combined_plan)
        
        return combined_plan


    
    
    
    
    
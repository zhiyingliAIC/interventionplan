
class disease_policy_data(object):
    def __init__(self, data, disease, physique = ""):
        self.disease = disease
        self.data = data
        self.physique = physique
    
    def get_right_data(self, label):
        flag = 0
        for d in self.data:
            if d["name"] == self.disease:
                flag = 1
                return d[label]
        if flag == 0:
            return []
        
    def get_tips(self):
        return self.get_right_data("tips")
            
            
    def get_should_eat(self):
        return self.get_right_data("shouldEat")
            
    def get_should_not_eat(self):
        return self.get_right_data("shouldNotEat")
            
    def get_exercise(self):
        return self.get_right_data("exercise")
    
    def get_acupoints(self):
        for d in self.data:
            if d["name"] == self.disease:
                for dd in d["withPhysique"]:
                    if dd["physique"] == self.physique:
                        return dd["point"]
        
        


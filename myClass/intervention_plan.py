import os
import sys
import json
from collections import OrderedDict
from datetime import datetime, timedelta
import random
from myClass.system_para import paths
import bisect

#current_dir = os.path.abspath(os.path.dirname(__file__))
#sys.path.append(current_dir)
#import system_para
from myClass.data_query import disease_policy_data

def getSeason(solar_term):
    season_map = {
        "春季":["立春","雨水","惊蛰","春分","清明","谷雨"],
        "夏季":["立夏","小满","芒种","夏至","小暑","大暑"],
        "秋季":["立秋","处暑","白露","秋分","寒露","霜降"],
        "冬季":["立冬","小雪","大雪","冬至","小寒","大寒"]
    }
    #find season
    season = ""
    for k,v in season_map.items():
        if solar_term in v:
            season = k
    return season
#=======================================================#

def getJson(path,filename):
    if filename == "insomniaHCPolicy.json":
        with open("%s/%s" % (path, filename) , "r") as f:
            json_data = json.load(f)
    else:
        with open("%s/%s" % (path, filename) , "r") as f:
            json_data = json.load(f)["data"]
        
    return json_data

#=======================================================#

def loadKnowledge(path):
    city_weather = getJson(path, "cityWeather.json")
    solar_term_awareness = getJson(path, "solarTermHCPolicy.json")
    disease_policy = getJson(path, "diseaseHCPolicy.json")
    physique_policy = getJson(path, "physiqueHCPolicy.json")
    issue_policy = getJson(path, "issueHCPolicy.json")
    weather_policy = getJson(path, "weatherHCPolicy.json")
    recipes = getJson(path, "recipe.json")
    material_property = getJson(path, "materialProperty.json")
    schedule = getJson(path, "schedule.json")
    exercise_prescription = getJson(path, "exercisePrescription.json")
    sports = getJson(path, "sports.json")
    exercise_tips = getJson(path, "exerciseTips.json")
    acupoint_prescription = getJson(path, "acupointsPrescription.json")
    insomnia_solution = getJson(path, "insomniaHCPolicy.json")
    
    #acupoint
    return(city_weather, solar_term_awareness,disease_policy,issue_policy,
           physique_policy,weather_policy,recipes,material_property,
           schedule, exercise_prescription, sports, exercise_tips,
           acupoint_prescription, insomnia_solution)
#=======================================================#

def get_recipe(recipes, name):
    return_val = OrderedDict()
    for d in recipes:
        if d["name"] == name:
            return_val.update({"name":name})
            return_val.update({"material":d["material"]})
            return_val.update({"method":d["method"]})
            return_val.update({"function":d["function"]})
            return_val.update({"precaution":d["precaution"]})
    return return_val
#=======================================================#
def cdf(weights):
    '''This function gets the cumulative distribution'''
    total = sum(weights)
    result = []
    cumsum = 0
    for w in weights:
        cumsum += w
        result.append(cumsum / total)
    return result
#=======================================================#
def choice(population, weights,k):
    '''This function randomly selects items from a list according to their weights'''
    if k == 0:
        return []
    assert len(population) == len(weights)
    cdf_vals = cdf(weights)
    return_val = []
    counter = 0
    for i in range(0,20):
        x = random.random()
        
        idx = bisect.bisect(cdf_vals, x)
        
        if population[idx] != []:
            random_number = random.randint(0,len(population[idx])-1)
            selected_item = population[idx][random_number]
            if selected_item not in return_val:
                return_val.append(selected_item)
                counter += 1
        
        if counter >= k:
            return return_val
#=======================================================#

#flag defines the type of intervention plans,

def dec_para(flag):
    def decorate(func):
        path_data = paths().path_data()
        (city_weather, solar_term_policy, disease_policy, issue_policy,
         physique_policy, weather_policy, recipes, material_property,
         schedule, exercise_prescription, sports, exercise_tips,
         acupoint_prescription, insomnia_solution) = loadKnowledge(path_data)
        def wrapper(*args, **kwargs):
            if flag == "tips":
                new_func = func(solar_term_policy, disease_policy, issue_policy, physique_policy)
            elif flag == "exercises":
                new_func = func(solar_term_policy, disease_policy, issue_policy, physique_policy,
                                exercise_prescription, sports, exercise_tips)
            elif flag == "acupoints":
                new_func = func(physique_policy, acupoint_prescription, issue_policy, insomnia_solution)
            elif flag == "tea":
                new_func = func(physique_policy, disease_policy, issue_policy, insomnia_solution, recipes)
            elif flag == "foodTreatment":
                new_func = func(physique_policy, disease_policy, issue_policy, insomnia_solution, recipes)
            elif flag == "food":
                new_func = func(physique_policy, disease_policy, issue_policy, weather_policy,
                                solar_term_policy, insomnia_solution, material_property, recipes)
            elif flag == "insomnia":
                new_func = func(insomnia_solution)
            else:
                new_func = func(weather_policy)
            return new_func
        return wrapper
    return decorate


#=======================================================#

class tips(object):
    def __init__(self, user_feature,region_feature):
        '''the user_feature here is for a single user'''
        self.user_feature = user_feature
        self.region_feature = region_feature
    @dec_para("tips")
    def get_knowledge(solar_term_policy, disease_policy, issue_policy, physique_policy):
        return (solar_term_policy, disease_policy, issue_policy, physique_policy)
        
    def main(self):
        history = []
        tips_pool_user = []
        (solar_term_policy, disease_policy, issue_policy, physique_policy) = self.get_knowledge()
        
        for d in self.user_feature["disease"]:
            tips_pool_user.append(disease_policy_data(disease_policy, d).get_tips())
        
        
        for d in self.user_feature["healthProblemToday"]:
            tips_pool_user.append(disease_policy_data(issue_policy, d).get_tips())
        
        if tips_pool_user != [] and tips_pool_user is not None:
            tips_pool_user = [dd for d in tips_pool_user for dd in d]
        
        return_val_tip = OrderedDict()
        return_val_tip.update({"userID":self.user_feature["userID"]})
        tips_pool = []
        dict_tmp_tip = OrderedDict()  #stores the main dictionary for a particular day
        dict_tmp_tip.update({"dateSolar":self.region_feature["dateSolar"]})
        for d in solar_term_policy:
            if self.region_feature["solarTermAlgorithm"] == d["name"]:
                tips_pool.append(d["tips"])
        for d in physique_policy:
            if self.user_feature["physique"] == d["name"]:
                tips_pool.append(d["tips"])
        
        tips_pool.append(tips_pool_user)
        tips_pool = [dd for d in tips_pool for dd in d]
        while True:
            tip = tips_pool[random.randint(0,len(tips_pool)-1)]
            if tip not in history or len(history) >= len(tips_pool):
                break
        history.append(tip)
        dict_tmp_tip.update({"tip":tip})
        return_val_tip.update({"details":dict_tmp_tip})
        return return_val_tip
            
#=======================================================#

class exercises(object):
    def __init__(self, user_feature,region_feature):
        '''the user_feature here is for a single user'''
        self.user_feature = user_feature
        self.region_feature = region_feature
    @dec_para("exercises")
    def get_knowledge(solar_term_policy, disease_policy, issue_policy, physique_policy,
                      exercise_prescription, sports, exercise_tips):
        return (solar_term_policy, disease_policy, issue_policy, physique_policy,
                exercise_prescription, sports, exercise_tips)

    def get_target_heartrate(self):
        if self.user_feature["age"] == "":
            return "没有年龄，无法确定目标心率。"
        age = int(self.user_feature["age"])
        disease = self.user_feature["disease"]
        factor = 0.65
        if age <= 55 and disease == []:
            factor = 0.85
        elif age >55 and age < 65:
            if "高血压" not in disease and "冠心病" not in disease and "肿瘤" not in disease:
                factor = 0.7
            else:
                factor = 0.6
        elif age >= 65 and age < 75:
            factor = 0.6
        elif age >=75:
            factor = 0.5
        return_val = str(int((220.0 - float(age)) * factor))
        return return_val
    
        
    def main(self):
        history_tips = []
        history_sports = []
        tip_pool_user = []
        sport_pool = []
        return_val = OrderedDict()
        sport = ""
        sports = []
        (solar_term_policy, disease_policy, issue_policy, physique_policy,
        exercise_prescription, sports_description, exercise_tips) = self.get_knowledge()
        indicator = "" #if a user got diseases, choose diseases, if not, choose physique
        BMI = self.user_feature["BMI"]
        
        
        #get the candidate sports            
        #here, we don't consider health problems. The recommendation is based on disease, physique, and BMI.
        
        if self.user_feature["disease"] == []:
            if self.user_feature["physique"] == "":
                indicator = "平和"
            else:
                indicator = self.user_feature["physique"]
            for dd in exercise_prescription:
                if ((indicator == dd["name"] and self.user_feature["gender"] == "女") or 
                    (indicator == dd["name"] and self.user_feature["gender"] == "男" and dd["sport"] != "舞蹈")):
                    sport_pool.append(dd["sport"])
        else:
            for d in self.user_feature["disease"]:  #!!!!can be optimized, here we haven't consider the overlapping effects of multiple diseases
                indicator = d
                for dd in disease_policy:
                    if d == dd["name"]:
                        tip_pool_user.append(dd["exercise"])  #get time independent tips for exercise
                    
                for dd in exercise_prescription:
                    if ((d == dd["name"] and self.user_feature["gender"] == "女") or 
                    (d == dd["name"] and self.user_feature["gender"] == "男" and dd["sport"] != "舞蹈")) and dd["sport"] not in sport_pool:
                        sport_pool.append(dd["sport"])
        if BMI >= 28.0:
            if "跳绳" in sport_pool:
                sport_pool.remove("跳绳")
            if "爬山" in sport_pool:
                sport_pool.remove("爬山")
                
        for d in physique_policy:
            if self.user_feature["physique"] == d["name"]:
                tip_pool_user.append(d["tips"])

        tip_pool_user.append(exercise_tips)
        tip_pool_user = [dd for d in tip_pool_user for dd in d]
        
        return_val.update({"userID":self.user_feature["userID"]})
        
        day = self.region_feature
        dict_tmp_exercise = OrderedDict()  #stores the main dictionary for a particular day
        dict_tmp_exercise.update({"dateSolar":day["dateSolar"]})
            
        sports = []
        while True:
            sport = sport_pool[random.randint(0,len(sport_pool) - 1)]
            if sport not in sports:
                sports.append(sport)
            if len(sports) >=2:
                break
            
        for i, sp in enumerate(sports):  #get the details of a particular sport
            dict_tmp = OrderedDict()
            for ep in exercise_prescription:
                if ep["name"] == indicator and ep["sport"] == sp:
                    dict_tmp.update({"sport":sp})
                    dict_tmp.update({"targetHeartRate":self.get_target_heartrate()})
                    dict_tmp.update({"intensity":ep["intensity"]})
                    dict_tmp.update({"timeFrequency":ep["timeFrequency"]})
                    dict_tmp.update({"precaution":ep["precaution"]})
                    break
            key = "presciption%s" % str(i+1)   
            dict_tmp_exercise.update({key:dict_tmp})
          
        tip = ""
        tip_pool = []
        for sp in sports:
            for sd in sports_description:
                if sp == sd["name"]:
                    tip_pool.append(sd["tips"])
            
            
        for d in solar_term_policy:
            if day["solarTermAlgorithm"] == d["name"]:
                tip_pool.append(d["exercise"])
            
        tip_pool.append(tip_pool_user)
        tip_pool = [dd for d in tip_pool for dd in d]
        while True:
            tip = tip_pool[random.randint(0,len(tip_pool)-1)]
            if tip not in history_tips or len(history_tips) >= len(tip_pool) - 1:
                break
        history_tips.append(tip)
            
        dict_tmp_exercise.update({"tip":tip})
        return_val.update({"details":dict_tmp_exercise})
        return return_val     

#=======================================================#

class acupoints(object):
    def __init__(self, user_feature,region_feature):
        '''the user_feature here is for a single user'''
        self.user_feature = user_feature
        self.region_feature = region_feature

    @dec_para("acupoints")
    def get_knowledge(physique_policy, acupoint_prescription, issue_policy, insomnia_solution):
        return (physique_policy, acupoint_prescription, issue_policy, insomnia_solution)
    
    def main(self):
        (physique_policy, acupoint_prescription, issue_policy, insomnia_solution) = self.get_knowledge()
        return_val = OrderedDict()
        return_val.update({"userID":self.user_feature["userID"]})
        day = self.region_feature
        if self.user_feature["insomnia_type"] != "无" and self.user_feature["insomnia_type"] != "":
            dict_tmp = OrderedDict()
            dict_tmp.update({"dateSolar":day["dateSolar"]})
            #get random massage solution
            idx = random.randint(0,(len(insomnia_solution["massage"])-1))

            dict_tmp.update({"massage":insomnia_solution["massage"][idx]})
            for d in insomnia_solution["withType"]:
                if self.user_feature["insomnia_type"] == d["name"]:
                    dict_tmp.update({"acuPoints":d["acuPoints"]})
        else:
            if self.user_feature["disease"] != []:   #here we only deal with the situation of one disease
             #   disease = "".join(self.user_feature["disease"])
                acupoint = []
                for d in acupoint_prescription:
                    if d["name"] in self.user_feature["disease"]:
                        acupoint = d["prescription"]
                dict_tmp = OrderedDict()
                dict_tmp.update({"dateSolar":day["dateSolar"]})
                dict_tmp.update({"acuPoints":acupoint})
            elif "高血糖" in self.user_feature["healthProblemToday"] or "血压过高" in self.user_feature["healthProblemToday"]:
                
                acupoint = []
                for d in acupoint_prescription:
                    if d["name"] in self.user_feature["healthProblemToday"]:
                        acupoint = d["prescription"]
                dict_tmp = OrderedDict()
                dict_tmp.update({"dateSolar":day["dateSolar"]})
                dict_tmp.update({"acuPoints":acupoint})
            
            else:
                if self.user_feature["physique"] == "":
                    physique = "平和"
                else:
                    physique = self.user_feature["physique"]
                
                dict_tmp = OrderedDict()
                dict_tmp.update({"dateSolar":day["dateSolar"]})
                    
                season = getSeason(day["solarTermAlgorithm"])
                
                for d in physique_policy:
                    if d["name"] == physique:
                        for dd in d["withSeason"]:
                            if dd["season"] == season:
                                dict_tmp.update({"acuPoints":dd["acuPoint"]})
                                
        return_val.update({"details":dict_tmp})
        
        return return_val

#=======================================================#

class tea(object):
    def __init__(self, user_feature, region_feature):
        '''haven't added the function of giving recommendation based on history yet, for example,
        after 15 days, the system should recommend a different tea.
        '''
        self.user_feature = user_feature
        self.region_feature = region_feature
        
    @dec_para("tea")
    def get_knowledge(physique_policy, diease_policy, issue_policy, insomnia_solution, recipes):
        return (physique_policy, diease_policy, issue_policy, insomnia_solution, recipes)
    
    def get_disease_tea(self,disease_policy, recipes, disease):
        return_val = OrderedDict()
        for d in disease_policy:
            if d["name"] == disease:
                index_selected = random.randint(0, (len(d["tea"])-1))
                return_val = get_recipe(recipes, d["tea"][index_selected])
                return return_val
                
    def get_physique_tea(self,physique_policy, recipes, physique, season):
        return_val = OrderedDict()
        for d in physique_policy:
            if d["name"] == physique:
                for dd in d["withSeason"]:
                    if dd["season"] == season:
                        index_selected = random.randint(0, (len(dd["tea"])-1))
                        return_val = get_recipe(recipes, dd["tea"][index_selected])
                        return return_val
                                
    def get_insomnia_tea(self, insomnia_solution, recipes):
        return_val = OrderedDict()
        for d in insomnia_solution["withType"]:
            if self.user_feature["insomnia_type"] == d["name"]:
                if self.user_feature["disease"] == [] and self.user_feature["physique"] == []:
                    index_selected = random.randint(0,(len(d["tea"])-1))
                    data_selected = d["tea"][index_selected]
               
                else:
                    while True:
                        index_selected = random.randint(0,(len(d["tea"])-1))
                        data_selected = d["tea"][index_selected]
                    
                        not_suit_physique = data_selected["notSuitPhysique"]
                        not_suit_disease = data_selected["notSuitDisease"]
                        if self.user_feature["disease"][0] not in not_suit_disease and self.user_feature["physique"] not in not_suit_physique:
                            break
                        
                return_val.update(get_recipe(recipes,data_selected["name"]))
                return return_val

        
    def main(self):
        '''This module has realized the function of including history data yet.
           A rigorous method should check the history data and select those teas which are not recommended to users recently.
        '''
        (physique_policy, diease_policy, issue_policy, insomnia_solution, recipes) = self.get_knowledge()
        return_val = OrderedDict()
        return_val.update({"userID":self.user_feature["userID"]})
        day = self.region_feature
        dict_tmp = OrderedDict()
        dict_tmp.update({"dateSolar":day["dateSolar"]})
        if self.user_feature["insomnia_type"] != "无" and self.user_feature["insomnia_type"] != "":
            dict_tmp.update(self.get_insomnia_tea(insomnia_solution, recipes))
        else:
            if self.user_feature["disease"] != []:
                for disease in self.user_feature["disease"][:1]: #so far we only focus on one disease
                    dict_tmp.update(self.get_disease_tea(diease_policy, recipes, disease))
                    
            elif self.user_feature["healthProblemToday"] != []:
                for problem in self.user_feature["healthProblemToday"][:1]: #so far we only focus on one health problem
                    dict_tmp.update(self.get_disease_tea(issue_policy, recipes, problem))
       
            else:
                if self.user_feature["physique"] == "":
                    physique = "平和"
                else:
                    physique = self.user_feature["physique"]
                
                dict_tmp = OrderedDict()
                dict_tmp.update({"dateSolar":day["dateSolar"]})
                    
                season = getSeason(day["solarTermAlgorithm"])
                
                dict_tmp.update(self.get_physique_tea(physique_policy, recipes, physique, season))
                                
        return_val.update({"details":dict_tmp})
        
        return return_val

#=====================================================

class foodTreatment(object):
    
    '''this module returns a food treatment recipe, which is different from a regular food recipe. Here we haven't realized the
    function of recommending food treatment for a fixed amount of time.'''
    
    def __init__(self, user_feature, region_feature):
        '''the user_feature here is for a single user'''
        self.user_feature = user_feature
        self.region_feature = region_feature
        
    @dec_para("foodTreatment")
    def get_knowledge(physique_policy, disease_policy, issue_policy, insomnia_solution, recipes):
        return (physique_policy, disease_policy, issue_policy, insomnia_solution, recipes)
    
    
    def get_disease_food(self,disease_policy, recipes, disease):
        return_val = OrderedDict()
        for d in disease_policy:
            if d["name"] == disease:
                index_selected = random.randint(0, (len(d["foodPrescription"])-1))
                return_val = get_recipe(recipes,d["foodPrescription"][index_selected])
                return return_val
                
    def get_physique_food(self,physique_policy, recipes, physique, season):
        return_val = OrderedDict()
        for d in physique_policy:
            if d["name"] == physique:
                for dd in d["withSeason"]:
                    if dd["season"] == season:
                        index_selected = random.randint(0, (len(dd["foodPrescription"])-1))
                        return_val = get_recipe(recipes, dd["foodPrescription"][index_selected])
                        return return_val
            
                        
    def get_insomnia_food(self, insomnia_solution, recipes):
        return_val = OrderedDict()
        for d in insomnia_solution["withType"]:
            if self.user_feature["insomnia_type"] == d["name"]:
                if self.user_feature["disease"] == [] and self.user_feature["physique"] == []:
                    index_selected = random.randint(0,(len(d["foodPrescription"])-1))
                    data_selected = d["foodPrescription"][index_selected]
               
                else:
                    while True:
                        index_selected = random.randint(0,(len(d["foodPrescription"])-1))
                        data_selected = d["foodPrescription"][index_selected]
                        not_suit_physique = data_selected["notSuitPhysique"]
                        not_suit_disease = data_selected["notSuitDisease"]
                        if self.user_feature["disease"][0] not in not_suit_disease and self.user_feature["physique"] not in not_suit_physique:
                            break
                        
                return_val.update(get_recipe(recipes,data_selected["name"]))
                return return_val
      
    def main(self):
        (physique_policy, diease_policy, issue_policy, insomnia_solution, recipes) = self.get_knowledge()
        return_val = OrderedDict()
        return_val.update({"userID":self.user_feature["userID"]})
        day = self.region_feature
        dict_tmp = OrderedDict()
        dict_tmp.update({"dateSolar":day["dateSolar"]})
        if self.user_feature["insomnia_type"] != "无" and self.user_feature["insomnia_type"] != "":            
            dict_tmp.update(self.get_insomnia_food(insomnia_solution,recipes))
        else:
            if self.user_feature["disease"] != []:
                for disease in self.user_feature["disease"][:1]: #so far we only focus on one disease
                    dict_tmp.update(self.get_disease_food(diease_policy, recipes, disease))
            elif self.user_feature["healthProblemToday"] != []:
                for problem in self.user_feature["healthProblemToday"][:1]:
                    dict_tmp.update(self.get_disease_food(issue_policy, recipes, problem))
                
            else:
                if self.user_feature["physique"] == "":
                    physique = "平和"
                else:
                    physique = self.user_feature["physique"]
                                   
                season = getSeason(day["solarTermAlgorithm"])            
                dict_tmp.update(self.get_physique_food(physique_policy,recipes,physique,season))
                                
        return_val.update({"details":dict_tmp})
        
        return return_val
#=====================================================

class food(object):
    
    '''this module returns a food treatment recipe, which is different from a regular food recipe. Here we haven't realized the
    function of recommending food treatment for a fixed amount of time.'''
    
    def __init__(self, user_feature, region_feature):
        '''the user_feature here is for a single user'''
        self.user_feature = user_feature
        self.region_feature = region_feature
        
    @dec_para("food")
    def get_knowledge(physique_policy, disease_policy, issue_policy, weather_policy,
                    solar_term_policy, insomnia_solution, material_property, recipes):
        return (physique_policy, disease_policy, issue_policy, weather_policy,
                solar_term_policy, insomnia_solution, material_property, recipes)
    
    def filter_single(self,key,feature_data,policy_data):
        return_val = []
        if key == "physique" or key == "solarTermAlgorithm":
            for policy in policy_data:
                if feature_data[key] == policy["name"]:
                    return_val = policy["shouldNotEat"]
        else:
            for feature in feature_data[key]:
                for policy in policy_data:
                    if feature == policy["name"]:
                        return_val = policy["shouldNotEat"]
                
        return return_val
        
    def filter_food_material(self,day):
      #  print (self.region_feature)
        (physique_policy, disease_policy, issue_policy, weather_policy,
        solar_term_policy, insomnia_solution, material_property, recipes) = self.get_knowledge()
        material_total = []
        
        region_map = {
            "山东":["济南"],
            "广东":["广州"],
            "江苏":["南京"]
        } #!!! can be optimized
        
        season = getSeason(day["solarTermAlgorithm"])
        #find region
        region = ""
        for k,v in region_map.items():
            if self.user_feature["region"] in v:
                region = k
        for material in material_property:
            material_total.append(material["name"])
        
        length_threshold = 10  #defines the threshold for the number of kinds of food.
        disease_material = self.filter_single("disease",self.user_feature,disease_policy)
        issue_material = self.filter_single("healthProblemToday",self.user_feature,issue_policy)
        physique_material = self.filter_single("physique",self.user_feature,physique_policy)
        solar_term_material = self.filter_single("solarTermAlgorithm",day,solar_term_policy)
        weather_material = self.filter_single("cityWeather",day,weather_policy)
        name_selected = []
        name_filterd_out = []
        for d in material_total:
            if d not in disease_material and d not in issue_material \
            and d not in physique_material and d not in solar_term_material\
            and d not in weather_material:
                name_selected.append(d)
            else:
                name_filterd_out.append(d)
        material_selected = OrderedDict()
        material_selected.update({"grain":[]})
        material_selected.update({"bean":[]})
        material_selected.update({"meat":[]})
        material_selected.update({"egg":[]})
        material_selected.update({"seafood":[]})
        material_selected.update({"veggie":[]})
        material_selected.update({"fruit":[]})
        material_selected.update({"nut":[]})
        material_selected.update({"drink":[]})
        
        for name in name_selected:
            for d in material_property:
                if name == d["name"] and (d["growSeason"] == [] or season in d["growSeason"]):
                  if d["region"] == [] or region in d["region"]:  
                    if d["type"] == "谷物":
                        material_selected["grain"].append(name)
                    elif d["type"] == "豆类":
                        material_selected["bean"].append(name)
                    elif d["type"] == "肉类":
                        material_selected["meat"].append(name)
                    elif d["type"] == "蛋类":
                        material_selected["egg"].append(name)
                    elif d["type"] == "水产":
                        material_selected["seafood"].append(name)
                    elif d["type"] == "蔬菜":
                        material_selected["veggie"].append(name)
                    elif d["type"] == "水果":
                        material_selected["fruit"].append(name)
                    elif d["type"] == "坚果":
                        material_selected["nut"].append(name)
                    elif d["type"] == "饮品":
                        material_selected["drink"].append(name)
        return (material_selected,name_filterd_out)
    
    def cal_weight(self,feature_json):
        
        '''this function returns a json which has the following format:
           {"history":{
               "benefit":0.36,
               "random":0.24
               },
            "weather":{
               "benefit":0.18,
               "random":0.12
            },
            ......
           }
           
           feature_json is a json file with feature's name as the key and feature's value as value
        '''
        w_benefit = 0.6  #每一个feature，如体质，食材会有三种不同的对应方式：有利于，无所谓，不利于，其中不利于的情况已从一开始过滤掉了，现在是给有利于及无所谓情况的权重
        w_random = 0.4
        
        w_feature = {
            "issue":5.0,
            "disease":1.5,
            "physique": 2.0,
            "weather": 0.75,
            "solar": 0.75
        }
        w_feature_new = OrderedDict()
        normalize_factor = 0.0  #we need to normalize different weights when there are factors missing
        if feature_json["weather"] != "":
            normalize_factor = normalize_factor + w_feature["weather"]
            w_feature_new.update({"weather":w_feature["weather"]})
        else:
            w_feature_new.update({"weather":0.0})
        if feature_json["disease"] != []:
            normalize_factor = normalize_factor + w_feature["disease"]
            w_feature_new.update({"disease":w_feature["disease"]})
        else:
            w_feature_new.update({"disease":0.0})
        if feature_json["issue"] != []:
            normalize_factor = normalize_factor + w_feature["issue"]
            w_feature_new.update({"issue":w_feature["issue"]})
        else:
            w_feature_new.update({"issue":0.0})
        if feature_json["physique"] != "":
            normalize_factor = normalize_factor + w_feature["physique"]
            w_feature_new.update({"physique":w_feature["physique"]})
        else:
            w_feature_new.update({"physique":0.0})
     #   if feature_json["symptom"] != []:
     #       normalize_factor = normalize_factor + w_feature["symptom"]
     #       w_feature_new.update({"symptom":w_feature["symptom"]})
     #   if feature_json["organ"] != []:
     #       normalize_factor = normalize_factor + w_feature["organ"]
     #       w_feature_new.update({"organ":w_feature["organ"]})
        w_feature_new.update({"solar":w_feature["solar"]})
        normalize_factor = normalize_factor + w_feature["solar"]
        return_val = OrderedDict()
        for k, v in w_feature_new.items():
            dict_tmp = OrderedDict()
            dict_tmp.update({"benefit":round(v/normalize_factor*w_benefit,2)})
            dict_tmp.update({"random":round(w_random/float(len(w_feature_new)),2)})
            return_val.update({k:dict_tmp})
        return return_val
    
    def get_1feature_pool(self, data_json, feature_val,data_total):
        '''This function select a pool of food materials based on a particular feature.
        data_json is the policy json file, data_total is the food material list'''
        
        return_val_benefit = OrderedDict() #food material that is good for the user
        return_val_random = OrderedDict() #food material that is not bad for the user
        should_eat = {}
        flag = 0  #flag for checking if a disease can be found in the policy json data.
        for d in data_json:
            if d["name"] == feature_val:
                should_eat = d["shouldEat"]
                flag = 1

        if flag == 0:
            for k, v in data_total.items():
                return_val_benefit.update({k:[]})        
                return_val_random.update({k:[]})
        else:
            for k, v in data_total.items():
                list_b = []
                list_r = []
                for d in v:
                    if d in should_eat[k]:
                        list_b.append(d)
                    else:
                        list_r.append(d)
                return_val_benefit.update({k:list_b})        
                return_val_random.update({k:list_r})

        return (return_val_benefit,return_val_random)
    
    def get_population(self,material_selected,day,k,feature_weights):
        '''material_selected:materials after filetering food which is not suitable for a particular situation;
           day:regional feature
           k:grain,bean,meat....
           feature_weights:weights for different features
           
        '''
        (physique_policy, disease_policy, issue_policy, weather_policy,
        solar_term_policy, insomnia_solution, material_property, recipes) = self.get_knowledge()
        
        issue_b = OrderedDict()
        issue_r = OrderedDict()
        disease_b = OrderedDict()
        disease_r = OrderedDict()
        physique_b = OrderedDict()
        physique_r = OrderedDict()
        solar_term_b = OrderedDict()
        solar_term_r = OrderedDict()
        weather_b = OrderedDict()
        weather_r = OrderedDict()
        issues_b = []
        issues_r = []
        diseases_b = []
        diseases_r = []
        weathers_b = []
        weathers_r = []
        
        population = []
        weights = []
        keys = ["issue","disease","physique","weather","solar"]
        if self.user_feature["healthProblemToday"] == []:
            (issue_b, issue_r) = self.get_1feature_pool(issue_policy,"",material_selected)
            issues_b.append(issue_b)
            issues_r.append(issue_r)
        else:
            for d in self.user_feature["healthProblemToday"]:
                (issue_b, issue_r) = self.get_1feature_pool(issue_policy,d,material_selected)
                issues_b.append(issue_b)
                issues_r.append(issue_r)
        if self.user_feature["disease"] == []:
            (disease_b,disease_r) = self.get_1feature_pool(disease_policy,"",material_selected)
            diseases_b.append(disease_b)
            diseases_r.append(disease_r)
        else:
            for d in self.user_feature["disease"]:
                (disease_b,disease_r) = self.get_1feature_pool(disease_policy,d,material_selected)
                diseases_b.append(disease_b)
                diseases_r.append(disease_r)
        if self.user_feature["physique"] != "":
            (physique_b,physique_r) = self.get_1feature_pool(physique_policy,self.user_feature["physique"],material_selected)
        else:
            (physique_b,physique_r) = self.get_1feature_pool(physique_policy,"",material_selected)
        (solar_term_b,solar_term_r) = self.get_1feature_pool(solar_term_policy,day["solarTermAlgorithm"],material_selected)
        for d in day["cityWeather"]:
            if d != "平":
                (weather_b,weather_r) = self.get_1feature_pool(weather_policy,d,material_selected)
                weathers_b.append(weather_b)
                weathers_r.append(weather_r)
            else:
                (weather_b,weather_r) = self.get_1feature_pool(weather_policy,"",material_selected)
                weathers_b.append(weather_b)
                weathers_r.append(weather_r)
        for d in keys:
            if d == "issue":
                population.append(issues_b[0][k])
                population.append(issues_r[0][k])
            if d == "disease":
                population.append(diseases_b[0][k])
                population.append(diseases_r[0][k])
            if d == "physique":
                population.append(physique_b[k])
                population.append(physique_r[k])
            if d == "weather":
                population.append(weathers_b[0][k])
                population.append(weathers_r[0][k])
            if d == "solar":
                population.append(solar_term_b[k])
                population.append(solar_term_r[k])
            weights.append(feature_weights[d]["benefit"])
            weights.append(feature_weights[d]["random"])
        return(population,weights)
    
    def find_main_material(self,name,recipes):
        for r in recipes:
            if name == r["name"]:
                return r["mainMaterial"]
            else:
                return []
            
    def best_recipe(self,materials,recipes,names_filtered_out):
        '''This function selects recipes with the food materials availabel, but should be optimized, we are not using it now.'''
        
        (physique_policy, disease_policy, issue_policy, weather_policy,
        solar_term_policy, insomnia_solution, material_property, recipes) = self.get_knowledge()
        
        threshold = 0.5  #the threshold for the percentage of food material found in 'materials' in a recipe
        percentage = []
        item_selected = []
        return_val = ""
        for d in recipes:
            counter_found = 0
            main_materials = self.find_main_material(d["name"],recipes)
            if main_materials == []:
                return_val = d
            else:
                for m in main_materials:
                    if m in materials and m not in names_filtered_out:
                        
                        counter_found += 1
                       
                percentage.append(float(counter_found)/float(len(main_materials)))
            
            for i, dd in enumerate(percentage):
                if dd >= threshold and d["type"] != "茶":
                    item_selected.append(recipes[i])
                    
        if item_selected == []:
            return_val = recipes[random.randint(0,len(recipes) - 1 )]
        else:
            return_val = item_selected[random.randint(0,len(item_selected) - 1 )]
        return return_val
            
        
    
    def get_meal_plan(self,choices,name_filtered_out):
        
        (physique_policy, disease_policy, issue_policy, weather_policy,
        solar_term_policy, insomnia_solution, material_property, recipes) = self.get_knowledge()
        
        breakfast = []
        lunch_dinner = []
     #   print (choices)
        for c in choices:
            for r in recipes:
                if c in r["mainMaterial"]:
                            
                    if ((r["type"] == "汤面"
                    or r["type"] == "馄饨" or r["type"] == "粥" 
                    or r["type"] == "饼" or r["type"] == "糕")):
                         breakfast.append(r["name"])
                    if ((r["type"] == "凉菜"
                    or r["type"] == "热菜" or r["type"] == "汤" 
                    or r["type"] == "包子" or r["type"] == "")):
                        lunch_dinner.append(r["name"])
        if breakfast == []:
            breakfast_selected.append("长寿粥")
        breakfast_selected = breakfast[random.randint(0,len(breakfast)-1)]
        idx_lunch = random.randint(0,int((len(lunch_dinner)-1)*0.5))
        lunch_selected = lunch_dinner[idx_lunch]
        idx_dinner = random.randint(int((len(lunch_dinner)-1)*0.5),(len(lunch_dinner)-1))
        dinner_selected = lunch_dinner[idx_dinner]

            
        
    #    print (breakfast_selected,lunch_selected,dinner_selected)
            
        return (breakfast_selected,lunch_selected,dinner_selected)
    
    def clean_recipe(self,data_dict):
                
        keys = ["name","material","method","function","precaution"]
        final_dict = {key: data_dict[key] for key in data_dict.keys() if key in keys}
        return final_dict
    
    def main(self):
      #  material_log = []  #the log for selected material
      
        (physique_policy, disease_policy, issue_policy, weather_policy,
        solar_term_policy, insomnia_solution, material_property, recipes) = self.get_knowledge()
        total_array = []#saves the dictionary for all days.
        
        return_val = OrderedDict()
        return_val.update({"userID":self.user_feature["userID"]})
        return_array = []
        num_material = {
            "grain": 3,
            "bean": 2,
            "meat": 2,
            "egg": 1,
            "seafood": 0.5,  #indicates 0 or 1
            "veggie": 3,
            "fruit": 2,
            "nut": 1,
            "drink": 1        
        }
        
        keys = ["grain","bean","meat","egg","seafood","veggie","fruit","nut","drink"]
        
        day = self.region_feature
        breakfast = []
        lunchDinner = []
        recipe_dict = OrderedDict()
            
        dict_tmp = OrderedDict()  #stores the main dictionary for a particular day
            
        dict_tmp.update({"dateSolar":day["dateSolar"]})
        material_dict = OrderedDict() #stores the dict of materials selected for a particular day
                
        (material_selected,name_filtered_out) = self.filter_food_material(day)
                    
            #combine user's feature and region feature
        combined_feature = OrderedDict()
        combined_feature.update({"issue":self.user_feature["healthProblemToday"]})
        combined_feature.update({"disease":self.user_feature["disease"]})
        combined_feature.update({"physique":self.user_feature["physique"]})
        combined_feature.update({"weather":day["cityWeather"]})
        combined_feature.update({"solar":day["solarTermAlgorithm"]})
            #calculate weights for different feature
        feature_weights = self.cal_weight(combined_feature)
        choices_total = []
        for k in keys: #keys are grain, meat and so on.
            weights = []
            population = []
            
                #!!!!!!!!!so far only single issue and disease are considered.
               # print (k,population)
                
            (population,weights) = self.get_population(material_selected,day,k,feature_weights)
                
                
            if num_material[k] == 0.5:
                num_daily_material = random.randint(0,1)
            else:
                num_daily_material = num_material[k]
                    
                #select materials based on weights
            choices = choice(population,weights,num_daily_material)
            array_tmp = []
            choices_total.append(choices)
                
            for c in choices:
                choice_tmp = OrderedDict()
                for m in material_property:
                    if m["name"] == c:
                        choice_tmp.update({"name":c})
                        choice_tmp.update({"function":m["functionDescription"]})
                        choice_tmp.update({"suitGroup":m["suitGroup"]})
                        choice_tmp.update({"precaution":m["precaution"]})
                        break
                array_tmp.append(choice_tmp)
            material_dict.update({k:array_tmp})
               
            #get recipes        
        (breakfast_selected,lunch_selected,dinner_selected) = self.get_meal_plan([tt for t in choices_total for tt in t],name_filtered_out)
            
        recipe_dict.update({"breakfast":get_recipe(recipes,breakfast_selected)})
        recipe_dict.update({"lunch":get_recipe(recipes,lunch_selected)})
        recipe_dict.update({"dinner":get_recipe(recipes,dinner_selected)})

        dict_tmp.update({"material":material_dict})
        dict_tmp.update({"recipe":recipe_dict})
        return_val.update({"details":dict_tmp})
    #    print (return_val)
        return return_val            
#=====================================================

class alert(object):
    def __init__(self,user_feature,region_feature):
        self.user_feature = user_feature
        self.region_feature = region_feature
        
    def main(self):
        day = self.region_feature
        return_val = OrderedDict()
        return_val.update({"userID":self.user_feature["userID"]})
        dict_tmp = OrderedDict()
        dict_tmp.update({"dateSolar":day["dateSolar"]})
        array_tmp = []
        for d in self.user_feature["healthProblemToday"]: 
            if d == "危险高血压":
                array_tmp.append("您的血压很高，请及时就医！")
            
            if d == "危险低血氧":
                array_tmp.append("您的血氧很低，请及时就医！")
            
            if d == "危险高烧":
                array_tmp.append("您的体温很高，请及时就医！")
        
        for d in self.user_feature["healthProblemDoctor"]:
            if d == "血压过高":
                array_tmp.append("您已经连续几天血压比正常值高了，请及时就医！")
            
            if d == "血压过低":
                array_tmp.append("您已经连续几天血压比正常值低了，请及时就医！")
            
            if d == "高血糖":
                array_tmp.append("您已经连续几天血糖比正常值高了，请及时就医！")
            
            if d == "低血糖":
                array_tmp.append("您已经连续几天血糖比正常值低了，请及时就医！")
            
            if d == "低血氧":
                array_tmp.append("您已经连续几天血氧比正常值低了，请及时就医！")
            
            if d == "发烧":
                array_tmp.append("您已经连续几天发烧了，请及时就医！")
        
        dict_tmp.update({"warning":array_tmp})
        return_val.update({"details":dict_tmp})
        
        return return_val
            
      #      
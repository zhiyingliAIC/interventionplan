from datetime import datetime
from datetime import timedelta
from datetime import date
import os

class myDates(object):
    """ The class calculates different dates needed by this program, including
        the date before the starting date of an intervention plan and
        the end date of the time window for the intervention plan.
        If there is a health issue detected by wearable devices,
        we need to update the intervention plan.
        Then this class also provides the end date for updating the intervention plan.
        
        Para:
            the number of days we need to update the intervention plan,
            due to the health issue raised by wearable devices
            so far, we chose a window of one week, then, N = 7
       
        Note:
            we are planning to generate the intervention plan after 12:00am,
            which means the infomation of users is in yesterday's data file.
            That's why we need the date before the starting date of the intervention plan.
       
        Args:
            starting_date: the date from command line, the starting date for an intervention plan, by default, it is today's date, String
            window_length: the time widow lenght for an intervention plan, String
            
        Attributes:
            yesterday: returns the date before the starting date, type: Datetime
            updating_date: returns the end date for updating the intervention plan
            end_date: returns the end date of the time window for the intervention plan
    """
    
    def __init__(self, date_starting, window_length):
        self.date_starting = datetime.strptime(date_starting, "%Y-%m-%d")    #format: yyyy-mm-dd so far, it is should be today's date
        self.window_length = int(window_length) - 1
    
    def starting_date(self):
        return self.date_starting
            
    def yesterday(self):
        #if the system is designed to generate new plan after midnight, then we need yesterday's historical data.
        return self.date_starting + timedelta(days = -1)
        
    def end_date(self):
        return self.date_starting + timedelta(days = self.window_length)
    
#=======================================================#

class paths(object):
    def __init__(self, date_starting=date.today(), userID=1):
        self.date_starting = datetime.strftime(date_starting,"%Y-%m-%d")
        self.userID = userID
    
    def path_root(self):
        path_pycode = os.path.dirname(os.path.abspath(__file__))
        path_pycode = path_pycode[:path_pycode.rfind("/")]
        return path_pycode
    
    def path_data(self):
        return "%s/systemData" % self.path_root()
    
    def path_input(self):
        path_root = self.path_root()
        return "%s/input/%s" % (path_root, self.userID)
    
    def path_output(self):
        path_root = self.path_root()
        path_userID = "%s/output/%s" % (path_root, self.userID)
        if not os.path.exists(path_userID):
            os.mkdir(path_userID)
        path_output = "%s/%s" % (path_userID, self.date_starting)
        if not os.path.exists(path_output):
            os.mkdir(path_output)
        return path_output
        
    def path_history(self):
        path_root = self.path_root()
        path_history = "%s/history/%s" % (path_root, self.userID)
        if not os.path.exists(path_history):
            os.mkdir(path_history)
        return path_history
        
        


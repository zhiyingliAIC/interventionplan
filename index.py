from flask import Flask, jsonify, abort, make_response, request
import json
from myClass.intervention_main import make_directory, daily_report

app = Flask(__name__)
 
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.route("/output/api/v1.0/daily", methods=["POST"])
def get_intervention():
    if not request.json:
        abort(400)
    
    user_info = request.json

    data_return = daily_report(user_info).main()
    
    return jsonify(data_return)
 
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
import os
import sys
import json
from collections import OrderedDict
from datetime import datetime
from myClass.system_para import myDates, paths

#=======================================================#
class personalFeature(object):
    '''Due to the lack of sleeping professionals, the module of evaluating insomnia type remains undeveloped.
       So far, the output of this module is a constant.
    '''
    def __init__(self, user_info):
        self.userID = user_info["userID"]
        self.birth_date = user_info["birthDate"]
        self.gender = user_info["gender"]
        self.region = user_info["region"]
        self.education = user_info["education"]
        self.occupation = user_info["occupation"]
        self.height = user_info["height"]
        self.weight = user_info["weightAve"]
        self.waistline = user_info["waistlineAve"]
        self.life_style = user_info["lifeStyle"]
        self.disease_history = user_info["diseaseHistory"]
        self.physique = user_info["constitution"]
        self.smart_device = user_info["smartDevice"]
        self.sleep_diary = user_info["sleepDiary"]
        self.date_device = user_info["sleepDevice"]["dateDeviceUTC"] #The date of data collected by a sleep device may be different from the
                                                                    #date of the daily report.
        self.sleep_device = user_info["sleepDevice"]  #this field may change.
        self.date_report = user_info["dateReportUTC"]
        self.return_fields = user_info["returnFields"]
        self.path_history = paths(datetime.strptime(self.date_report,"%Y-%m-%d"),self.userID).path_history()
        
        
    def get_age(self):
        age_group = {
            "婴儿":"0 3",
            "儿童":"3 10",
            "少年":"10 18",
            "青年":"18 41",
            "中年":"41 66",
            "老年":"66 200"
        }
        today = datetime.today()
        if self.birth_date == "":
            return ("","")
        else:
            birthdate = datetime.strptime(self.birth_date,"%Y-%m-%d")
            age = (today.year - birthdate.year)
            for k,v in age_group.items():
                age_min = int(v.split(" ")[0])
                age_max = int(v.split(" ")[1])
                if age >= age_min and age < age_max:
                    group = k
        return (age, group)
    
    def get_BMI(self):
        if self.height == 0 or self.weight == 0:
            return 0
        weight = float(self.weight)
        height = float(self.height)
        
        BMI = weight/height/height*10000.0
        return BMI
    
    def low_cal(self):
        '''this function returns the portion (from 0~1) of low calorie food material;
        if it is negative, it means the portion of high calorie food material;
        if it is 0, it means select food material randomly.'''
        
        BMI = self.get_BMI()
        gender = self.gender
        waistline = self.waistline
        if BMI == 0:
            return 0.0
        
        if BMI < 18.5:
            return -0.2
        
        if gender == "男":
            if BMI >= 18.5 and BMI <=23.9:
                if waistline < 85:
                    return 0.0
                elif waistline >= 85 and waistline <= 95:
                    return 0.3
                elif waistline > 95:
                    return 0.5
            if BMI >= 24.0 and BMI <=27.9:
                if waistline < 85:
                    return 0.3
                elif waistline >= 85 and waistline <= 95:
                    return 0.5
                elif waistline > 95:
                    return 0.7
            if BMI > 28.0:
                if waistline < 85:
                    return 0.5
                elif waistline >= 85 and waistline <= 95:
                    return 0.7
                elif waistline > 95:
                    return 0.7
        else:
            if BMI >= 18.5 and BMI <=23.9:
                if waistline < 80:
                    return 0.0
                elif waistline >= 80 and waistline <= 90:
                    return 0.3
                elif waistline > 90:
                    return 0.5
            if BMI >= 24.0 and BMI <=27.9:
                if waistline < 80:
                    return 0.3
                elif waistline >= 80 and waistline <= 90:
                    return 0.5
                elif waistline > 90:
                    return 0.7
            if BMI > 28.0:
                if waistline < 80:
                    return 0.5
                elif waistline >= 80 and waistline <= 90:
                    return 0.7
                elif waistline > 90:
                    return 0.7

    def feature_history(self,length):
        
        json_data = []
        full_filename = "%s/history_feature.json" % self.path_history
        if os.path.isfile(full_filename):
            if os.stat(full_filename).st_size > 0:
                with open(full_filename, "r") as f:
                    json_data = json.load(f)["data"]
            else:
                return []
        if len(json_data) < length:
            return []
        else:
            return json_data[-length:]
    
    def get_health_problem(self):
        
        disease = self.disease_history
        return_val_today = []  #the variable for storing health problems based on today's data
        return_val_doctor = [] #the variable for storing health problems based on 3-days' historical data
        
        
        #get historic data
        length = 3 #the number of days for evaluating long-term health problems
        his_data = self.feature_history(length)
        #coded name:
        #hbp - high blood pressure
        #lbp - low blood pressure
        #hbs - high blood sugar
        #lbs - low blood sugar
        #lbo - low blood oxygen
        #hr - heart rate
        #bt - body temperature
        if his_data == []:
            flag_hbp = False
            flag_lbp = False
            flag_hbs = False
            flag_lbs = False
            flag_lbo = False
            flag_ht = False
        else:
            flag_hbp = True
            for d in his_data:
                if "血压过高" not in d["healthProblemToday"] and "危险高血压" not in d["healthProblemToday"]:
                    flag_hbp = False
        
            flag_lbp = True
            for d in his_data:
                if "血压过低" not in d["healthProblemToday"]:
                    flag_lbp = False        
        
            flag_hbs = True
            for d in his_data:
                if "高血糖" not in d["healthProblemToday"]:
                    flag_hbs = False
                
            flag_lbs = True
            for d in his_data:
                if "低血糖" not in d["healthProblemToday"]:
                    flag_lbs = False
        
            flag_lbo = True
            for d in his_data:
                if "低血氧" not in d["healthProblemToday"] and "危险低血氧" not in d["healthProblemToday"]:
                    flag_lbo = False
                    
            flag_ht = True
            for d in his_data:
                if "发烧" not in d["healthProblemToday"] and "危险高烧" not in d["healthProblemToday"] and "高烧" not in d["healthProblemToday"]:
                    flag_ht = False
        
        #smart device
        bp_high = int(self.smart_device["bloodPressureHigh"])
        bp_low = int(self.smart_device["bloodPressureLow"])
        bs = float(self.smart_device["bloodSugar"])
        bo = int(self.smart_device["bloodOxygen"])
        hr = int(self.smart_device["heartRate"])
        bt = float(self.smart_device["bodyTemperature"])
        
        if (((bp_high >= 130 or bp_low >= 85 and (bp_high < 180 and bp_high > 0)) or
            (bp_high >= 130 or bp_low >= 85 and (bp_low < 110 and bp_low > 0))) and
            "高血压" not in disease and "糖尿病" not in disease and "高脂血" not in disease and "冠心病" not in disease):
            return_val_today.append("血压过高")
        if bp_high >= 180 and bp_low >= 110: 
            return_val_today.append("危险高血压")
        if bp_high >= 140 and bp_low >= 90 and ("糖尿病" in disease or "高脂血" in disease or "冠心病" in disease):
            return_val_today.append("危险高血压")
        if ((bp_high <= 90 and bp_high > 0) or (bp_low > 0 and bp_low <= 60)) and "低血压" not in disease:
            return_val_today.append("血压过低")
        if bs >= 6.1 and "糖尿病" not in disease:
            return_val_today.append("高血糖")
        if (bs > 0 and bs <= 2.8) and "糖尿病" not in disease:
            return_val_today.append("低血糖")
        if (bs > 0 and bs <= 3.9) and "糖尿病" in disease:
            return_val_today.append("低血糖")
        if (bo > 0 and bo <= 94) and bo > 90:
            return_val_today.append("低血氧")
        if bo < 90 and bo > 0:
            return_val_today.append("危险低血氧")
    #    if hr > 100 and "冠心病" not in disease:
    #        return_val_today.append("心率过速")  #心率部分需要完善。暂时不预警。也不干预。
    #    if hr > 80 and "高血压" in disease and "冠心病" not in disease:
    #        return_val_today.append("心率过速")
    #    if hr > 0 and hr < 60 and "冠心病" not in disease:
    #        return_val_today.append("心率过缓")
        if bt > 37.0 and (bt > 0 and bt <= 38.5):
            return_val_today.append("发烧")
        if bt > 38.5 and (bt > 0 and bt < 40.0):
            return_val_today.append("高烧")
        if bt >= 40.0:
            return_val_today.append("危险高烧")
        
        if "血压过高" not in return_val_today and "危险高血压" not in return_val_today:
            flag_hbp = False
            
        if "血压过低" not in return_val_today:
            flag_lbp = False
            
        if "高血糖" not in return_val_today:
            flag_hbs = False
        
        if "低血糖" not in return_val_today:
            flag_lbs = False
        
        if "低血氧" not in return_val_today and "危险低血氧" not in return_val_today:
            flag_lbo = False
            
        if "发烧" not in return_val_today and "危险高烧" not in return_val_today and "高烧" not in return_val_today:
            flag_ht = False
            
        if flag_hbp == True:
            return_val_doctor.append("血压过高")
            
        if flag_lbp == True:
            return_val_doctor.append("血压过低")
        
        if flag_hbs == True:
            return_val_doctor.append("高血糖")
            
        if flag_lbs == True:
            return_val_doctor.append("低血糖")
            
        if flag_lbo == True:
            return_val_doctor.append("低血氧")
        
        if flag_ht == True:
            return_val_doctor.append("发烧")
            
        return (return_val_today, return_val_doctor)
    
    def get_habit_problem(self):
        return_val = []
        if self.life_style["drink"] == "多":
            return_val.append("酗酒")
        if self.life_style["drink"] == "中":
            return_val.append("喝酒")
        if self.life_style["smoke"] == "多":
            return_val.append("酗烟")
        if self.life_style["smoke"] == "中":
            return_val.append("抽烟")
        if self.life_style["sedentary"] == "是":
            return_val.append("久坐")
        if self.life_style["vegetarian"] == "是":
            return_val.append("素食")
        return return_val
            
    ### we can extract more features from sleep diary.
    '''
    def get_risk(self):
        dr = self.risk_analysis["diabetesRisk"]
        hbpr = self.risk_analysis["highBloodPressureRisk"]
        hdr = self.risk_analysis["heartDiseaseRisk"]
        cr = self.risk_analysis["cancerRisk"]
        disease = self.disease_history
        risk_list = ["糖尿病", "高血压", "冠心病", "肿瘤"]
        
        risk = []
        if dr == "高" and "糖尿病" not in disease:
            risk.append("糖尿病")
        if hbpr == "高" and "高血压" not in disease:
            risk.append("高血压")
        if hdr == "高" and "冠心病" not in disease:
            risk.append("冠心病")
        if cr == "高" and "肿瘤" not in disease:
            risk.append("肿瘤")
        for k,v in self.family_history.items():
            for d in v:
                if d in risk_list:
                    risk.append(d)
        
        #room for medical report
        
        risk = list(set(risk))
        return risk
    '''
    def insomnia_type(self):
        '''Need some experts to give us suggestions to finish this part. The data format for this function
         will depend on specific devices, it may also include stress level, the time stamp for movement, and etc.
         '''
        TSData = self.sleep_device["sleepTS"]
        insomnia_type = ""
        return insomnia_type
        
            
    
   # def get_sleep_problem(self):
        # So far we only focus on the type of insomnia
        


     
    def main(self):
        return_val = OrderedDict()
        return_val.update({"userID":self.userID})
        return_val.update({"gender":self.gender})
        return_val.update({"disease":self.disease_history})
        return_val.update({"physique":self.physique})
        
        (age,group) = self.get_age()
        
        diet_rate = self.low_cal()
        
        health_problem_today = self.get_health_problem()[0]
        health_problem_doctor = self.get_health_problem()[1]
        habit_problem = self.get_habit_problem()
        insomnia_type = self.insomnia_type()
        

    #    risk = []
    #    for d in self.get_risk():
    #        if d not in self.disease_history:
    #            risk.append(d)
        BMI = self.get_BMI()   
        return_val.update({"ageGroup":group})
        return_val.update({"age":age})
        return_val.update({"region":self.region})
        return_val.update({"BMI":BMI})
        return_val.update({"dietRate":diet_rate})
        return_val.update({"healthProblemToday":health_problem_today})
        return_val.update({"healthProblemDoctor":health_problem_doctor})
        return_val.update({"habitProblem":habit_problem})
        return_val.update({"insomnia_type":insomnia_type})
        return_val.update({"dateReport":self.date_report})
        return_val.update({"dateDevice":self.date_device})
        return_val.update({"returnFields":self.return_fields})
#        return_val.update({"risk":risk})  #saved for later
        
        
        return return_val
    
class regionFeature(object):
    
    def __init__(self, date_info, region):
        self.date_info = date_info
        self.region = region
        self.path_data = paths().path_data()
        self.default = "默认值"
    
    def get_city_weather(self):
        with open("%s/cityWeather.json" % self.path_data, "r") as f:
            json_data = json.load(f)["data"]
        return json_data
                
    def get_regional_feature(self, city_weather, wylq):
        weather_type_final = []
        if wylq in city_weather:
            weather_type_final = city_weather
        elif wylq == "湿" and "燥" in city_weather:
            city_weather.remove("燥")
        elif wylq == "湿" and "风" in city_weather:
            city_weather.remove("风")
        elif (wylq == "湿" or wylq == "寒") and "火" in city_weather:
            city_weather.remove("火")
        elif (wylq == "燥" or wylq == "风" or wylq == "火") and "湿" in city_weather:
            city_weather.remove("湿")
        elif (wylq == "暑" or wylq == "火") and "寒" in city_weather:
            city_weather.remove("寒")
        elif wylq == "寒" and "暑" in city_weather:
            city_weather.remove("暑")
        else:
            city_weather.append(wylq)
        if "平" in city_weather:
            city_weather.remove("平")
        if city_weather == []:
            city_weather.append("平")
        weather_type_final = city_weather
        return weather_type_final
    
    def find_property(self, city_weather_total, region, solar_term, wylq):
        flag = 0
        for d in city_weather_total:
            if d["city"] == region:
                flag = 1
                for dd in d["weather"]:
                    if dd["solarTerm"] == solar_term:
                        city_weather = dd["weatherType"]
                        city_weather_final = self.get_regional_feature(city_weather, wylq)
        if flag == 1:                
            return city_weather_final
        else:
            return None
           
    def main(self):
        city_weather_total = self.get_city_weather()
        city_weather = []
        city_weather_final = []
        solar_term = self.date_info["solarTerm"]
        solar_term_algorithm = self.date_info["solarTermAlgorithm"]
        city_weather_final = self.find_property(city_weather_total, self.region, solar_term_algorithm, self.date_info["wylq"])
        if city_weather_final is None:
            city_weather_final = self.find_property(city_weather_total, self.default, solar_term_algorithm, self.date_info["wylq"])
                    
        region_features = OrderedDict()
        region_features.update({"dateSolar":self.date_info["dateSolar"]})
        region_features.update({"solarTermAlgorithm":solar_term_algorithm})
        region_features.update({"solarTerm":solar_term})
        region_features.update({"cityWeather":city_weather_final})
        
        return region_features